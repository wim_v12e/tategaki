# tategaki

A simple Perl script to transpose Japanese text from horizontal, left-to-right to vertical, right-to-left. Then name `tategaki` is the Japanese word for vertical writing, 縦書き.

It takes a text file (UTF-8) as input and prints the output in the terminal.

- You can define the number of columns per page and either the total number of characters per page or the number of characters per column.
- You can use a column spacer to improve readability.
- There is an option to fit the number of characters per column to the longest line in the horizontal version, if that is shorter than the number of characters per column.

Usage:  
		<Japanese text, written horizontally, utf-8 text file>
		[--nchars = number of characters per page (500)]
		[--ncols  = number of columns per page (14)]
		[--nrows = number of rows per page ]
		[--fit = fit the page to the text ]
		[--trim = remove redundant spaces ]
		[--spacer = THIN_SPACE (default), NARROW_NOBREAK_SPACE, SPACE, VERTICAL_LINE, NONE]

	- You can only provide either `nchars` or `nrows`
	- The `fit` option will fit the page size to the text if the longest line 
	  in the text is shorter than the set number of characters per column.
	  Set to `False` (Raku) or 0 (Perl) if you don't want this.
	- The `trim` option will remove redundant spaces.
	  Set to `False` (Raku) or 0 (Perl) if you don't want this.
	- The `spacer` adds a space character between the columns for better readability. 
	  By default this is the unicode THIN SPACE character.