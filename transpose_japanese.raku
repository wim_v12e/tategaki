#!/usr/bin/env raku

sub USAGE() {
    print Q:c:to/EOH/; 
    Usage: {$*PROGRAM-NAME} 
		<Japanese text, written horizontally, utf-8 text file>
		[--nchars = number of characters per page (500)]
		[--ncols  = number of columns per page (14)]
		[--nrows = number of rows per page ]
		[--fit = fit the page to the text ]
		[--trim = remove redundant spaces ]
		[--spacer = THIN_SPACE (default), NARROW_NOBREAK_SPACE, SPACE, VERTICAL_LINE, NONE]

	- You can only provide either `nchars` or `nrows`
	- The `fit` option will fit the page size to the text if the longest line 
	  in the text is shorter than the set number of characters per column.
	  Set to `False` if you don't want this.
	- The `trim` option will remove redundant spaces.
	  Set to `False` if you don't want this.
	- The `spacer` adds a space character between the columns for better readability. 
	  By default this is the unicode THIN SPACE character.
 
 EOH
}

enum Spacer (NONE => '', THIN_SPACE=>' ', NARROW_NOBREAK_SPACE=>' ', SPACE=>' ', VERTICAL_LINE=>'|') ;


# enum Spacer (NONE => '', THIN_SPACE=>' ', NARROW_NB_SPACE=>' ', SPACE=>' ', BAR=>'|') ;

unit sub MAIN(
  Str $file ,
  Int :$nchars = 500,
  Int :$ncols = 14,
  Int :$nrows,
  Bool :$fit = True,
  Bool :$trim = True,
  Spacer :$spacer = THIN_SPACE,
  Bool :$verbose = False
);  

if (not $file.defined) {
# USAGE();
     die "Please provide an input file\n";
	 
}
my $input_file = IO::Path.new( $file ) ;
my $V = $verbose;

my $fit_page_size = $fit;# True;

my $use_col_spacer = ($spacer.defined and !($spacer ~~ NONE));

my $col_space_char = $spacer.value;
# The defaults are for pages of 500 characters, for Mastodon
my $use_n_chars_per_page = not $nrows.defined;
my $n_chars_per_page = $nchars;
my $n_cols_per_page = $ncols;
my $n_chars_per_col = 35;
# If column spacers are used, effectively there will be 2*$n_cols_per_page-1 columns

# I pad with this many extra characters and simply hope that by stripping spaces at the end I will stay below $n_chars_per_page
# There is no technical reason for this, it's just so the example renders nicely.
$n_chars_per_page += 2*$n_cols_per_page-1 if $use_col_spacer and $trim;

$n_chars_per_col = $use_n_chars_per_page 
	?? $use_col_spacer 
		?? ($n_chars_per_page/(2*$n_cols_per_page-1)).floor
		!! ($n_chars_per_page/$n_cols_per_page).floor
	!! $use_col_spacer 
		?? ($n_chars_per_col*$n_cols_per_page/(2*$n_cols_per_page-1)).floor 
		!! $n_chars_per_col;

# '' (nothing, the default)
# thin space  U+2009   THIN SPACE (HTML &#8201; · &thinsp;). 
# non-breaking space U+202F   NARROW NO-BREAK SPACE (HTML &#8239;) is a non-breaking space with a width similar to that of the thin space. 
# Ordinary space
# '|'

my $col_spacer = $use_col_spacer ?? $col_space_char !! NONE.value;


my $pad_ws= $V ?? '＿' !! '　'; 
my @pad_col = $pad_ws xx $n_chars_per_col; 

my @lines = $input_file.IO.lines;

my $max_line_length = 0;
my @padded_lines = map {
    my $line = $_;
	my $line_length =  $line.chars;
	$max_line_length = $line_length > $max_line_length ?? $line_length !! $max_line_length;
	if ($line_length < $n_chars_per_col) {
		$line ~= $pad_ws x ($n_chars_per_col - $line_length);
	}
	$line;

},  @lines;

if ($fit_page_size and $max_line_length < $n_chars_per_col ) {
	# we can make the pages smaller
    @padded_lines = map {
        substr($_,0,$max_line_length);
    }, @padded_lines;
	$n_chars_per_col = $max_line_length;
}

# Join all strings then split into chars
my $all_chars_str = join('', @padded_lines);
# Very odd of Raku to add a '' at start and end when splitting a string into chars.
my @all_chars=split('',$all_chars_str,:skip-empty);

my @pages=();
my $n_cols=0;
my @cols_for_page=();
my $page_ctr=0;
while (@all_chars) {
	say @all_chars.elems if $V;
	my @col = $n_chars_per_col < @all_chars.elems
		?? @all_chars[0 .. $n_chars_per_col-1] 
        !! $n_chars_per_col == @all_chars.elems
            ?? @all_chars
            !! (@all_chars, $pad_ws xx ($n_chars_per_col - @all_chars.elems));
		# !! (@all_chars, split('', $pad_ws x ($n_chars_per_col - @all_chars.elems),:skip-empty  ));
    
	@all_chars = $n_chars_per_col < @all_chars.elems 
		?? @all_chars[$n_chars_per_col .. @all_chars.elems - 1] 
		!! ();

	push @cols_for_page, [@col];

	if (++$n_cols == $n_cols_per_page or @all_chars.elems == 0) {
		
        # pad to a full page
		if (@all_chars.elems == 0
		    and $n_cols < $n_cols_per_page
		) {
			my $n_pad_cols = $n_cols_per_page - $n_cols;
			for 1 .. $n_pad_cols {
				push @cols_for_page, [@pad_col];
			}
		}
        say 'Add cols to page: ' ~ @cols_for_page.perl if $V;
		push @pages, [@cols_for_page];		
		$n_cols=0;
		@cols_for_page=();
        say "Page {$page_ctr+1} done: " ~ @pages[$page_ctr++].perl if $V;
	}
		
}
say "Done!" if $V;

say @pages.perl if $V;

# Now transpose

for  @pages -> @page {
die if @page.elems==0;
	for 1..$n_chars_per_col {
		my @chars=();
		for reverse( @page) -> @col {
            next if @col.elems==0;
			my $char = shift @col;
			push @chars, $char;		
		}
		my $chars_str = join($col_spacer,@chars);
		if ($trim) {
			if ($use_col_spacer) {
			$chars_str ~~ s/[ $pad_ws || $col_space_char ]+$//;
			} else {
			$chars_str ~~ s/$pad_ws+$//;
			}
		}
		say $chars_str;
	}
	say '';
	#  say 'ー' x $n_cols_per_page;
}

