#!/usr/bin/env perl
use v5.28;
use warnings;
use strict;
use utf8;

use Data::Dumper;      

use Getopt::Long;


our $usage = <<EOH;
    Usage: $0 
		<Japanese text, written horizontally, utf-8 text file>
		[--nchars = number of characters per page (500)]
		[--ncols  = number of columns per page (14)]
		[--nrows = number of rows per page ]
		[--fit = fit the page to the text ]
		[--trim = remove redundant spaces ]
		[--spacer = THIN_SPACE (default), NARROW_NOBREAK_SPACE, SPACE, VERTICAL_LINE, NONE]

	- You can only provide either `nchars` or `nrows`
	- The `fit` option will fit the page size to the text if the longest line 
	  in the text is shorter than the set number of characters per column.
	  Set to `0` if you don't want this.
	- The `trim` option will remove redundant spaces.
	  Set to `0` if you don't want this.
	- The `spacer` adds a space character between the columns for better readability. 
	  By default this is the unicode THIN SPACE character.

EOH

our ($NONE, $THIN_SPACE, $NARROW_NONBREAK_SPACE, $SPACE, $VERTICAL_LINE) =  ('',' ',' ',' ','|');

# my $input_file;
my $length = 24;
my $fit_page_size = 1;
my $use_col_spacer = 1;
my $col_space_char = $THIN_SPACE;
# The defaults are for pages of 500 characters, for Mastodon
my $use_n_chars_per_page = 1;
my $n_chars_per_page = 500;
my $n_cols_per_page = 14;
my $n_chars_per_col;# = 35;
my $trim=1;
my $col_space_char_str='THIN_SPACE';

my $V=0;
my $help=0;
GetOptions (
	"nchars=i" => \$n_chars_per_page,  
	"ncols=i" => \$n_cols_per_page,
	"nrows=i" => \$n_chars_per_col,
	"fit=i" => \$fit_page_size,
	"trim=i" => \$trim,
	"spacer=s" => \$col_space_char_str,
	# "file=s"   => \$input_file,      # 'string
	"verbose"  => \$V,
	"help" => \$help
	)   # flag
or die usage()."\n";
if ($help) {
	die $usage;
}

# This creates a variable from a string with the name of the variable
no strict 'refs';
$col_space_char = $$col_space_char_str;
use strict 'refs';

$use_col_spacer = 0 if $col_space_char eq $NONE;
$use_n_chars_per_page = 0 if defined $n_chars_per_col;
# If column spacers are used, effectively there will be 2*$n_cols_per_page-1 columns
$n_chars_per_page += 2*$n_cols_per_page-1 if $use_col_spacer and $trim;

$n_chars_per_col = $use_n_chars_per_page 
	? $use_col_spacer 
		? int($n_chars_per_page/(2*$n_cols_per_page-1))
		: int($n_chars_per_page/$n_cols_per_page) 
	: $use_col_spacer 
		? int($n_chars_per_col*$n_cols_per_page/(2*$n_cols_per_page-1)) 
		: $n_chars_per_col;

# '' (nothing, the default)
# thin space  U+2009   THIN SPACE (HTML &#8201; · &thinsp;). 
# non-breaking space U+202F   NARROW NO-BREAK SPACE (HTML &#8239;) is a non-breaking space with a width similar to that of the thin space. 
# Ordinary space
# '|'

my $col_spacer = $use_col_spacer ? $col_space_char : $NONE;

binmode STDOUT, ':utf8';
#binmode STDERR, ':utf8';

if (not @ARGV)  {
    die $usage."\n";
}
my $input_file = $ARGV[0];

my $pad_ws = $V ? '＿' : '　';
my $pad_col_ws_str=$pad_ws x $n_chars_per_col;
my @pad_col = split('', $pad_col_ws_str);


open my $IN, "<:encoding(UTF-8)", $input_file or die $!;

# my $all_chars_str='';
my @lines=();
my $max_line_length = 0;
my @padded_lines=();
while (my $line=<$IN>) {
    chomp $line;
	my $line_length = length($line);
	$max_line_length = $line_length > $max_line_length ? $line_length : $max_line_length;
	
	if ($line_length < $n_chars_per_col) {
		$line .= $pad_ws x ($n_chars_per_col - $line_length);
	}
	push @padded_lines, $line;
}
close $IN;

if ($fit_page_size and $max_line_length < $n_chars_per_col ) { die 'BOOM!';
	# we can make the pages smaller
	for my $line (@padded_lines) {
		$line = substr($line,0,$max_line_length);
	}
	$n_chars_per_col = $max_line_length;
}


my $all_chars_str = '';
map {$all_chars_str .= $_} @padded_lines;



my @all_chars=split('',$all_chars_str);
map {say $_} @all_chars if $V;
my @pages=();
my $n_cols=0;
my @cols_for_page=();


while (@all_chars) {
	say scalar @all_chars if $V;
	my @col = $n_chars_per_col < scalar @all_chars 
		? @all_chars[0..$n_chars_per_col - 1] 
		: $n_chars_per_col == scalar @all_chars
			? @all_chars
			: (@all_chars, split('', $pad_ws x ($n_chars_per_col - scalar(@all_chars)) )) ;
	@all_chars = $n_chars_per_col < scalar @all_chars 
		? @all_chars[$n_chars_per_col .. $#all_chars - 1] 
		: ();

	push @cols_for_page, [@col];
	if (++$n_cols == $n_cols_per_page or scalar @all_chars == 0) {
		say 'Add cols to page: '.Dumper(@cols_for_page) if $V;
		if (scalar @all_chars == 0
		and $n_cols < $n_cols_per_page
		) {
			my $n_pad_cols = $n_cols_per_page - $n_cols;
			for (1 .. $n_pad_cols) {
				push @cols_for_page, [@pad_col];
			}
		}
		push @pages, [@cols_for_page];		
		$n_cols=0;
		@cols_for_page=();
	}
		say 'Page done: '.Dumper(@pages) if $V;
}
say "Done!" if $V;

say Dumper(@pages) if $V;

# Now transpose

for my $page (@pages) {
	my $char_count=0;
	for (1..$n_chars_per_col) {
		my @chars=();
		for my $col (reverse @{$page}) {
			my $char = shift @{$col};
			push @chars, $char;		
		}
		my $chars_str = join($col_spacer,@chars);
		if ($trim) {
		$chars_str=~s/[$pad_ws$col_space_char]+$//;
		}
		say $chars_str;
		$char_count+=length($chars_str);
	}
	# say $char_count;
	say '';
	# say 'ー' x $n_cols_per_page;
}


